/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer''
|
*/

import Route from '@ioc:Adonis/Core/Route'
import { schema } from '@ioc:Adonis/Core/Validator'
import Url from 'App/Models/Url'
import crypto from 'crypto'

Route.get('/', async ({ view, request }) => {
  return view.render('welcome', {
    url: request.input('url'),
  })
})

Route.get('/:code', async ({ params, response }) => {
  let url = await Url.findBy('shortened', params.code)
  if (url) {
    return response.redirect(url.original)
  }
  return response.notFound('Shortened URL not found')
})

Route.post('/', async ({ request, view, response }) => {
  let validated = await request.validate({
    schema: schema.create({
      url: schema.string(),
    }),
  })

  // Check for missing http(s)
  if (!/^https?:\/\//i.test(validated.url)) {
    validated.url = 'http://' + validated.url
  }

  // Generate random shortened URL
  // In case of collisions try to recreate it up to 10 times
  // Kinda stupid yet simple way to do it..
  let shortened = ''
  let exists: null | Url = null
  let tries = 0
  while (!exists && tries <= 10) {
    shortened = crypto.randomBytes(5).toString('hex')
    exists = await Url.findBy('shortened', shortened)
    tries++
  }

  // In case couldn't generate unique URL in 10 times abort with error
  if (exists && tries === 10) {
    return response.internalServerError('Could not generate shortened URL')
  }

  // Insert new URL to DB
  const insertedUrl = await Url.create({
    original: validated.url,
    shortened,
  })

  return view.render('welcome', {
    url: insertedUrl.shortened,
  })
})
