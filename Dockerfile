# Build AdonisJS
FROM node:18-alpine as builder
# Set directory for all files
WORKDIR /home/node
# Copy over package.json files
COPY package*.json ./
# Install all packages
RUN npm ci
# Copy over source code
COPY . .
# Build AdonisJS for production
RUN npm run build --production

# Build final runtime container
FROM node:18-alpine
# Set environment variables
ENV NODE_ENV=production
# Disable .env file loading
ENV ENV_SILENT=true
# Set app key at start time
ENV HOST=0.0.0.0
ENV PORT=3333
ENV APP_NAME=url-shortener
ENV CACHE_VIEWS=true
ENV SESSION_DRIVER=cookie
ENV DRIVE_DISK=local
ENV DB_CONNECTION=sqlite
ENV APP_KEY=

# Set working directory
WORKDIR /home/node
# Copy over required files from previous steps
# Copy over built files
COPY --from=builder /home/node/build .
# Copy over package.json files
COPY package*.json ./
# Install packages
RUN npm ci --omit=dev
# Expose port 3333 to outside world
EXPOSE 3333
# Start server up
CMD [ "node", "server.js" ]
